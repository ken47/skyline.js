/**
You are viewing a skyline from an angle such that every building
appears as a rectangle to you, and these rectangles may overlap
depending on their distance from your vantage point.

You are given an array of buildings, each of which is described by
3 data points:
1) x: integer - its position in the 2d plane
2) w: integer - its width
3) h: integer - its height

Your task is to calculate the perimeter and area of this skyline, given the
aforementioned array. The ground is not considered part of the skyline's
perimeter.
 */

var Building = function(x, w, h) {
  this.x = x;
  this.w = w;
  this.h = h;
}
var buildings = [
  new Building(0, 9, 3),
  new Building(0, 6, 6),
  new Building(7, 4, 4)
];

var getSkyline = function(buildings) {
  var skyline = [];

  for(var i = buildings.length; i--;) {
    var b = buildings[i];
    for(var x = b.x; x < b.x + b.w; x++) {
      if (!skyline[x] || b.h > skyline[x]) {
        skyline[x] = b.h;
      }
    }
  }

  for(var i = skyline.length; i--;) {
    if (!skyline[i]) {
      skyline[i] = 0;
    }
  }

  return skyline;
}

var calculateArea = function(skyline) {
  var area = 0;

  for(var i = skyline.length; i--;) {
    area += skyline[i];
  }

  return area;
}

var calculatePerimeter = function(skyline) {
  var total = 0;
  var lastMax = 0;

  for(var i = skyline.length; i--;) {
    if (lastMax !== skyline[i]) {
      total += Math.abs(skyline[i] - lastMax);
      lastMax = skyline[i];
    } 
    if (skyline[i] !== 0) {
      total += 1;
    }
  }

  total += skyline[0];
  return total;
}

var skyline = getSkyline(buildings);

var perimeter = calculatePerimeter(skyline);
console.log('Perimeter: ' + perimeter);

var area = calculateArea(skyline);
console.log('Area: ' + area);
